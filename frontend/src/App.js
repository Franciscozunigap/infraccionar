import React from 'react'

import { BrowserRouter, Route, Routes } from 'react-router-dom'

import Inspector from './Components/InspectorSesion/Inspector'
import Infraccion from './Components/InfraccionVer/Infraccion'
import Inicio from './Components/Inicio'
import Cajero from './Components/CajeroSesion/Cajero'
import Juez from './Components/JuezSesion/Juez'

const App = () => {
    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/inspector' element={<Inspector />} />
                    <Route path='/infraccion' element={<Infraccion />} />
                    <Route path='/cajero' element={<Cajero />} />
                    <Route path='/juez' element={<Juez />} />
                    <Route path='/' element={<Inicio />} />
                </Routes>
            </BrowserRouter>
        </>
    )
}

export default App;