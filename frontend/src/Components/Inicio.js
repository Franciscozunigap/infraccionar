import * as React from 'react';
import Button from '@mui/material/Button/index';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import { ThemeProvider } from '@mui/material/styles';

import LocalPoliceOutlinedIcon from '@mui/icons-material/LocalPoliceOutlined';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import AdbIcon from '@mui/icons-material/CarCrash';
import AttachMoneyIcon from '@mui/icons-material/AttachMoneyOutlined';
import GavelIcon from '@mui/icons-material/GavelOutlined';

import { Link } from 'react-router-dom'

import { themeInspector, themeInfraccion, themeMunicipalidad } from './theme'
import { Box } from '@mui/material';

const Inicio = () => {

	return (
		<>
			<Stack mt={10} alignItems='center' direction="column" spacing={4}>
				<Box sx={{ display: 'flex', alignItems: 'left', flexDirection: 'column' }}>
					<Box sx={{ display: 'flex', alignItems: 'center', flexDirection: 'row' }}>
						<AdbIcon sx={{ display: 'flex', mr: 1, height: 32, width: 32 }} />
						<Typography
							variant="h5"
							noWrap
							sx={{
								mr: 2,
								display: 'flex',
								color: 'inherit',
								fontWeight: 700,
								fontFamily: 'monospace',
								letterSpacing: '.3rem',
							}}
						>
							SOFTWARE
						</Typography>
					</Box>
					<Typography
						variant='h3'
						noWrap
						sx={{
							display: 'flex',
							fontFamily: 'monospace',
							fontWeight: 700,
							color: 'inherit',
							textDecoration: 'none',
							mb: 4
						}}
					>
						INFRACCIONAR
					</Typography>
				</Box>
				<ThemeProvider theme={themeInfraccion}>
					<Link to='/infraccion'>
						<Button sx={{ height: 56, width: 320, fontWeight: 'normal' }} variant="contained" startIcon={<RemoveRedEyeIcon />}> Detalles Infracción </Button>
					</Link>
				</ThemeProvider>
				<Stack sx={{ width: 320, gap: 2 }}>
					<Typography textAlign='center' variant='h6' sx={{
						fontFamily: 'monospace',
						fontWeight: 700,
						letterSpacing: '.1rem'
					}}>
						MUNICIPALIDAD
					</Typography>
					<ThemeProvider theme={themeInspector}>
						<Link to='/inspector'>
							<Button sx={{ height: 56, width: 320, fontWeight: 'normal' }} variant="contained" startIcon={<LocalPoliceOutlinedIcon />} >
								Inspector
							</Button>
						</Link>
					</ThemeProvider>
					<ThemeProvider theme={themeMunicipalidad}>
						<Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
							<Link to='/juez'>
								<Button sx={{ height: 56, width: 140, fontWeight: 'normal', color: 'black' }} variant="contained" startIcon={<GavelIcon />}> Juez </Button>
							</Link>
							<Link to='/cajero'>
								<Button sx={{ height: 56, width: 140, fontWeight: 'normal', color: 'black' }} variant="contained" startIcon={<AttachMoneyIcon />}> Cajero </Button>
							</Link>
						</Box>
					</ThemeProvider>
				</Stack>
			</Stack>

		</>
	)
}

export default Inicio;