import * as React from 'react'
import { Link } from 'react-router-dom'

import infraccionService from '../../Services/Infracciones'
import apelacionService from '../../Services/Apelaciones'

import { Button, Stack, Typography } from '@mui/material'

import DetalleInfraccion from './DetalleInfraccion'
import BuscadorInfraccion from './BuscadorInfraccion'

const Infraccion = () => {
    const [infracciones, setInfracciones] = React.useState(null)
    const [rut, setRUT] = React.useState(null)

    const getInfracciones = React.useCallback(async () => {
        if (rut) {
            const infraccionesData = await infraccionService.getByRut(rut)
            if (infraccionesData) {
                setInfracciones(infraccionesData.reverse())
            } else {
                setInfracciones(null)
            }
        }
    }, [rut, setInfracciones])

    React.useEffect(() => {
        getInfracciones()
    }, [getInfracciones])

    const addApelacion = async (newApelacion) => {
        await apelacionService.create(newApelacion)
        getInfracciones()
    }

    const updateEstadoApelado = async (idInfraccion) => {
        await infraccionService.updateEstado(idInfraccion, 'apelado')
        getInfracciones()
    }

    const updateEstadoPagado = async (idInfraccion) => {
        await infraccionService.updateEstado(idInfraccion, 'pagado')
        await apelacionService.setRevisada(idInfraccion)
        getInfracciones()
    }

    return (
        <>
            <Link to='/'>
                <Button variant='text' sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}> Volver </Button>
            </Link>
            <Stack mt={rut === null ? 8 : infracciones === null ? 8 : 4} alignItems='center' justifyContent='space-between' direction='column'>
                {
                    rut === null
                        ? <BuscadorInfraccion setRUT={setRUT} />
                        : infracciones === null
                            ? <Typography> Infracciones no disponibles.
                                <Button
                                    variant='text'
                                    sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}
                                    onClick={() => setRUT(null)}
                                >
                                    Reingresar rut
                                </Button>
                            </Typography>
                            : <DetalleInfraccion
                                addApelacion={addApelacion}
                                updateEstadoPagado={updateEstadoPagado}
                                updateEstadoApelado={updateEstadoApelado}
                                infracciones={infracciones}
                                rut={rut}
                            />
                }

            </Stack>
        </>
    )
}

export default Infraccion