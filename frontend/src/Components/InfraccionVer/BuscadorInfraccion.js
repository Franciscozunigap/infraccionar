import { Box, TextField, Button } from '@mui/material'
import DescriptionIcon from '@mui/icons-material/Description';

const BuscadorInfraccion = ({ setRUT }) => {
    const handleSubmit = (event) => {
        event.preventDefault()
        const data = new FormData(event.currentTarget);
        const rut = data.get('rutInfractor')
        setRUT(rut)
    }

    return (
        <Box
            onSubmit={handleSubmit} component="form" noValidate autoComplete="off"
            sx={{
                '& .MuiTextField-root': { width: '30ch' },
                display: 'flex',
                alignItems: 'center',
                gap: 1,
            }}
        >
            <TextField name='rutInfractor' autoFocus id="rut_infractor" label="RUT" type="search" placeholder='XXXXXXXX-X' />
            <Button type='submit' sx={{ height: 56, fontFamily: 'monospace', fontWeight: '700', letterSpacing: '.1rem' }} variant="contained" endIcon={<DescriptionIcon />}>
                Ver
            </Button>
        </Box>
    )
}

export default BuscadorInfraccion