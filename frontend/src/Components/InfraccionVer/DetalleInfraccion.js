import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Container from '@mui/material/Container';
import { Typography, Stack, Button, Box, TextField } from '@mui/material';

import utility from '../../Utility/utility'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.info.dark,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const DetalleInfraccion = ({ addApelacion, updateEstadoApelado, updateEstadoPagado, infracciones, rut }) => {
    const [infraccion, setInfraccion] = React.useState(null)

    const handleSubmit = (event) => {
        event.preventDefault()

        const data = new FormData(event.currentTarget)
        const detalle = data.get('apelacion')
        addApelacion({ id_infraccion: infraccion.id, detalle })
        updateEstadoApelado(infraccion.id)
        setInfraccion({ ...infraccion, estado: 'apelado' })
    }

    const pagarInfraccion = () => {
        if (window.confirm('Confirmas el pago de ' + infraccion.monto + ' UTM')) {
            updateEstadoPagado(infraccion.id)
            setInfraccion(null)
        }
    }

    const detalleInfraccion = (inf) => {
        setInfraccion(inf)
    }

    return (
        <Container sx={{ justifyContent: 'space-between', gap: 4, display: 'flex', flexDirection: 'column' }}>
            <Stack>
                <Typography
                    variant="h5"
                    textAlign='center'
                    sx={{ fontFamily: 'Montserrat, sans-serif', fontWeight: '700' }}
                > Rut usuario: {rut} </Typography>
                <Typography color='grey' align='center' fontSize={12}> No recargar la página. </Typography>
            </Stack>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="customized table">
                    <TableHead>
                        <TableRow>
                            <StyledTableCell> N° </StyledTableCell>
                            <StyledTableCell align="center"> Tipo&nbsp; </StyledTableCell>
                            <StyledTableCell align="center"> Fecha </StyledTableCell>
                            <StyledTableCell align="center"> Patente&nbsp; </StyledTableCell>
                            <StyledTableCell align="center"> Motivo&nbsp; </StyledTableCell>
                            <StyledTableCell align="center"> Ubicación&nbsp; </StyledTableCell>
                            <StyledTableCell align="center"> Estado&nbsp; </StyledTableCell>
                            <StyledTableCell align="right"> Detalle Pago&nbsp; </StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {infracciones.map((infraccion) => {
                            return (
                                <StyledTableRow
                                    key={infraccion.id}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <StyledTableCell component="th" scope="row"> {infraccion.id} </StyledTableCell>
                                    <StyledTableCell align="center"> {infraccion.tipo} </StyledTableCell>
                                    <StyledTableCell align="center"> {utility.invertirString(infraccion.fecha_infraccion.substring(0, 10))} </StyledTableCell>
                                    <StyledTableCell align="center"> {infraccion.patente} </StyledTableCell>
                                    <StyledTableCell align="center"> {infraccion.motivo} </StyledTableCell>
                                    <StyledTableCell align="center"> {infraccion.ubicacion} </StyledTableCell>
                                    <StyledTableCell align="center"> {infraccion.estado} </StyledTableCell>
                                    <StyledTableCell align="right">
                                        <Button
                                            variant='outlined'
                                            onClick={() => detalleInfraccion(infraccion)}
                                            sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}
                                        >
                                            Ver
                                        </Button>
                                    </StyledTableCell>
                                </StyledTableRow>
                            )
                        }
                        )}
                    </TableBody>
                </Table>
            </TableContainer>
            {
                infraccion === null
                    ? null
                    : <Stack display='flex' flexDirection='column' gap={2} justifyContent='center'>
                        <Typography variant='h6' textAlign='center' sx={{ fontFamily: 'Montserrat, sans-serif', fontWeight: '700' }}> Infracción N° {infraccion.id} </Typography>
                        <Stack display='flex' flexDirection='row' justifyContent='center' gap={4}>
                            <Box display='flex' flexDirection='column' justifyContent='start' gap={1}>
                                <Typography> Monto: {infraccion.monto} (UTM) </Typography>
                                <Typography>
                                    {
                                        infraccion.estado !== 'pagado'
                                            ? 'Vence el ' + utility.invertirString(infraccion.fecha_vencimiento.substring(0, 10))
                                            : 'Pagado el ' + utility.invertirString(infraccion.fecha_pago.substring(0, 10))
                                    }
                                </Typography>
                                <Typography> Estado: {infraccion.estado} </Typography>
                            </Box>
                            {
                                infraccion.estado === 'emitido'
                                    ? <Box onSubmit={handleSubmit} component='form' display='flex' flexDirection='column' gap={1}>
                                        <TextField required name='apelacion' multiline label='Apelación' sx={{ width: 240 }} />
                                        <Button type='submit' sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}> Enviar Apelación </Button>
                                    </Box>
                                    : null
                            }
                            {
                                infraccion.estado !== 'pagado'
                                    ? <Button
                                        variant='contained'
                                        onClick={pagarInfraccion}
                                        sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}
                                    >
                                        Pagar
                                    </Button>
                                    : null
                            }
                        </Stack>
                    </Stack>
            }
        </Container>
    )
}

export default DetalleInfraccion