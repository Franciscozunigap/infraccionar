import React, { useEffect, useState, useCallback } from 'react';

import { Navigate } from 'react-router-dom'

import Login from '../Login';
import ResponsiveAppBar from '../ResponsiveAppBar';
import IngresarInfraccion from './IngresarInfraccion';
import InfraccionesDiarias from './InfraccionesDiarias';

import infraccionService from '../../Services/Infracciones'

import { Grid, Stack } from '@mui/material';

const Inspector = () => {
    const [user, setUser] = useState(null)
    const [infracciones, setInfracciones] = useState([])
    const tipo = 'inspector'

    const fechaString = new Date().toLocaleDateString()

    const getAllInfracciones = useCallback(async () => {
        if (user) {
            const allInfracciones = await infraccionService.getByUserID(user.id);
            setInfracciones(allInfracciones === null ? [] : allInfracciones.reverse());
        }
    }, [user]); // Dependencia user

    useEffect(() => {
        const loggedUserJSON = window.localStorage.getItem('loggedUser');
        if (loggedUserJSON) {
            const user = JSON.parse(loggedUserJSON);
            setUser(user);
        }
    }, [setUser]);

    useEffect(() => {
        // Incluye getAllInfracciones en este efecto solo cuando sea necesario
        if (user) {
            getAllInfracciones();
        }
    }, [user, getAllInfracciones]); // Dependencias de este efecto

    const addInfraccion = async (newInfraccion) => {
        await infraccionService.create(newInfraccion)
        getAllInfracciones()
    }

    if (user) {
        if (user.tipo !== tipo) {
            return <Navigate to='/' />
        }
    }

    return (
        <div className="App">
            {
                user === null
                    ?
                    <Login tipo={tipo} setUser={setUser} />
                    :
                    <>
                        <Stack
                            direction="column"
                            justifyContent="flex-start"
                            alignItems="stretch"
                            spacing={4}
                        >
                            <ResponsiveAppBar tipo={tipo} user={user} setUser={setUser} />
                            <Grid
                                container
                                direction="row"
                                justifyContent="space-between"
                                alignItems="flex-start"
                                spacing={2}
                            >
                                <Grid item xs={5} marginLeft={5}>
                                    <IngresarInfraccion addInfraccion={addInfraccion} user={user} />
                                </Grid>
                                <Grid item xs={5} marginRight={5}>
                                    <InfraccionesDiarias infracciones={infracciones} fecha={fechaString} />
                                </Grid>
                            </Grid>
                        </Stack>
                    </>
            }
        </div>
    );
}

export default Inspector