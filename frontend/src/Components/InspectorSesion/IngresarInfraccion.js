import {
    Typography,
    TextField,
    FormControl,
    InputLabel,
    FormControlLabel,
    Radio,
    RadioGroup,
    FormLabel,
    Button,
    OutlinedInput,
} from "@mui/material";

import UploadIcon from '@mui/icons-material/Upload';
import { useState } from "react";

const IngresarInfraccion = ({ addInfraccion, user }) => {
    const [newInfraccion, setNewInfraccion] = useState({
        rut_infractor: '',
        email_infractor: '',
        patente: '',
        motivo: '',
        ubicacion: '',
        tipo: '',
        monto: '',
        estado: '',
        id_trabajador: '',
    })

    const handleRutInfractor = (event) => setNewInfraccion({ ...newInfraccion, rut_infractor: event.target.value })
    const handleEmailInfractor = (event) => setNewInfraccion({ ...newInfraccion, email_infractor: event.target.value })
    const handlePatente = (event) => setNewInfraccion({ ...newInfraccion, patente: event.target.value })
    const handleMotivo = (event) => setNewInfraccion({ ...newInfraccion, motivo: event.target.value })
    const handleUbicacion = (event) => setNewInfraccion({ ...newInfraccion, ubicacion: event.target.value })
    const handleTipo = (event) => setNewInfraccion({ ...newInfraccion, tipo: event.target.value })
    const handleMonto = (event) => setNewInfraccion({ ...newInfraccion, monto: event.target.value === '' ? '' : parseFloat(event.target.value) })

    const handleSubmit = (event) => {
        event.preventDefault();

        addInfraccion({
            ...newInfraccion,
            estado: 'emitido',
            id_trabajador: parseInt(user.id),
        })

        setNewInfraccion({
            rut_infractor: '',
            email_infractor: '',
            patente: '',
            motivo: '',
            ubicacion: '',
            tipo: '',
            monto: '',
            estado: '',
            id_trabajador: '',
        })
    }

    return (
        <div>
            <Typography
                component='h1'
                variant="h5"
                textAlign='center'
                sx={{ fontWeight: '700' }}
            >
                Ingresar una nueva infracción:
            </Typography>
            <form onSubmit={handleSubmit}>
                <FormControl fullWidth required sx={{ mt: 2 }}>
                    <FormLabel id="infraccion-tipo"> Tipo </FormLabel>
                    <RadioGroup
                        row
                        aria-labelledby="infraccion-tipo"
                        value={newInfraccion.tipo}
                        onChange={handleTipo}
                        defaultValue={"leve"}

                    >
                        <FormControlLabel value="leve" control={<Radio />} label="Leve" />
                        <FormControlLabel value="moderada" control={<Radio />} label="Moderada" />
                        <FormControlLabel value="grave" control={<Radio />} label="Grave" />
                    </RadioGroup>
                </FormControl>
                <TextField
                    id="infraccion-rut"
                    value={newInfraccion.rut_infractor}
                    onChange={handleRutInfractor}
                    sx={{ mt: 2 }}
                    label="Rut Infractor"
                    multiline
                    required
                    fullWidth
                    variant='outlined'
                    placeholder="XXXXXXXX-X"
                />
                <TextField
                    id="infraccion-email"
                    value={newInfraccion.email_infractor}
                    onChange={handleEmailInfractor}
                    sx={{ mt: 2 }}
                    label="E-mail Infractor"
                    type="email"
                    multiline
                    required
                    fullWidth
                    variant="outlined"
                />
                <TextField
                    value={newInfraccion.patente}
                    onChange={handlePatente}
                    id="infraccion-patente"
                    sx={{ mt: 2 }}
                    label="Patente Vehículo"
                    multiline
                    required
                    fullWidth
                    placeholder="XX XX XX"
                    variant="outlined"
                />
                <TextField
                    id="infraccion-infraccion"
                    value={newInfraccion.motivo}
                    onChange={handleMotivo}
                    sx={{ mt: 2 }}
                    label="Motivo Infracción"
                    multiline
                    required
                    fullWidth
                    variant="outlined"
                />
                <TextField
                    value={newInfraccion.ubicacion}
                    onChange={handleUbicacion}
                    id="infraccion-ubicacion"
                    sx={{ mt: 2 }}
                    label="Ubicación"
                    multiline
                    required
                    fullWidth
                    variant="outlined"
                />
                <FormControl required fullWidth sx={{ mt: 2 }} variant='outlined'>
                    <InputLabel htmlFor="infraccion-multa" sx={{ backgroundColor: '#ffffff', px: 1 }}> Monto </InputLabel>
                    <OutlinedInput
                        id="infraccion-multa"
                        value={newInfraccion.monto}
                        onChange={handleMonto}
                        type="number"
                        endAdornment='UTM'
                    />
                </FormControl>
                <Button
                    type="Submit"
                    color="success"
                    sx={{ mt: 2, fontFamily: 'monospace', fontWeight: '700', letterSpacing: '.1rem', fontSize: 16 }}
                    variant="contained"
                    endIcon={<UploadIcon />}
                >
                    Ingresar
                </Button>
            </form>
        </div>

    )
};

export default IngresarInfraccion;