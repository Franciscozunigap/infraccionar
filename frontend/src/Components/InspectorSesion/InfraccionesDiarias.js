import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

const InfraccionesDiarias = ({ infracciones, fecha }) => {
    const [expanded, setExpanded] = React.useState(false);

    const handleChange = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    return (
        <div>
            <Typography
                component='h1'
                variant="h5"
                textAlign='center'
                sx={{ fontWeight: '700', mb: 5 }}
            >
                Reporte de infracciones {fecha}
            </Typography>
            {
                infracciones.length === 0
                    ? <Typography textAlign='center' sx={{ color: 'text.secondary' }}> No se registran infracciones hoy. </Typography>
                    :
                    infracciones.map((infraccion, i) => {
                        return (
                            <Accordion key={infraccion.id} expanded={expanded === `panel${i}`} onChange={handleChange(`panel${i}`)}>
                                <AccordionSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    aria-controls={`panel${i}bh-content`}
                                    id={`panel${i + 1}bh-header`}
                                >
                                    <Typography sx={{ width: '50%', flexShrink: 0 }}>
                                        {infraccion.patente}
                                    </Typography>
                                    <Typography sx={{ color: 'text.secondary' }}> {infraccion.tipo} </Typography>
                                </AccordionSummary>
                                <AccordionDetails>
                                    <Typography>
                                        rut: {infraccion.rut_infractor}
                                    </Typography>
                                    <Typography>
                                        motivo: {infraccion.motivo}
                                    </Typography>
                                    <Typography>
                                        ubicacion: {infraccion.ubicacion}
                                    </Typography>
                                    <Typography>
                                        {infraccion.email_infractor}
                                    </Typography>
                                </AccordionDetails>
                            </Accordion>
                        )
                    })
            }
        </div>
    )
};

export default InfraccionesDiarias;