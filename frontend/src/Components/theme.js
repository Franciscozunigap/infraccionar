import { green, orange, grey, blue } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

export const themeInspector = createTheme({
	palette: {
		primary: green,
	},
	typography: {
		button: {
			fontWeight: 'bold',
		},
	},
})

export const themeInfraccion = createTheme({
	palette: {
		primary: orange,
		secondary: grey,
	},
	typography: {
		button: {
			fontWeight: 'bold',
		},
	},
})

export const themeMunicipalidad = createTheme({
	palette: {
		primary: blue,
	},
	typography: {
		button: {
			fontWeight: 'bold',
		},
	},
})