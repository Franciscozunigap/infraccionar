import React, { useEffect, useState, useCallback } from 'react';

import { Navigate } from 'react-router-dom'

import Login from '../Login';
import ResponsiveAppBar from '../ResponsiveAppBar';

import Revision from './Revision';
import OutlinedCard from './OutlinedCard'

import infraccionService from '../../Services/Infracciones';
import apelacionService from '../../Services/Apelaciones';

import { Grid, Stack, Typography } from '@mui/material';

const Juez = () => {
    const [user, setUser] = useState(null)
    const [apelaciones, setApelaciones] = useState([])
    const [apelacion, setApelacion] = useState(null)
    const [infraccion, setInfraccion] = useState(null)
    const tipo = 'juez'

    const getAllApelaciones = useCallback(async () => {
        if (user) {
            const allApelaciones = await apelacionService.getJuzgado()
            setApelaciones(allApelaciones)
        }
    }, [user]);

    useEffect(() => {
        const loggedUserJSON = window.localStorage.getItem('loggedUser');
        if (loggedUserJSON) {
            const user = JSON.parse(loggedUserJSON);
            setUser(user);
        }
    }, [setUser]);

    useEffect(() => {
        if (user) {
            getAllApelaciones();
        }
    }, [user, getAllApelaciones])

    const setRevision = async (apelacion) => {
        setApelacion(apelacion)
        const data = await infraccionService.getSingle(apelacion.id_infraccion)
        setInfraccion(data[0])
    }

    const aprobarApelacion = async (observacion, monto) => {
        console.log(infraccion.monto)
        const objeto = {
            id: apelacion.id,
            id_infraccion: apelacion.id_infraccion,
            observacion: observacion,
            monto: monto === '' ? parseFloat(infraccion.monto) : parseFloat(monto),
        }
        console.log(objeto.monto)
        await apelacionService.aprobarApelacion(objeto)
        setInfraccion(null)
        setApelacion(null)
        getAllApelaciones()
    }

    const rechazarApelacion = async (observacion) => {
        const objeto = {
            id: apelacion.id,
            id_infraccion: apelacion.id_infraccion,
            observacion: observacion,
        }
        console.log(objeto)
        await apelacionService.rechazarApelacion(objeto)
        setInfraccion(null)
        setApelacion(null)
        getAllApelaciones()
    }

    if (user) {
        if (user.tipo !== tipo) {
            return <Navigate to='/' />
        }
    }

    return (
        <div className="App">
            {
                user === null
                    ?
                    <Login tipo={tipo} setUser={setUser} />
                    :
                    <>
                        <Stack
                            direction="column"
                            justifyContent="flex-start"
                            alignItems="stretch"
                            spacing={4}
                        >
                            <ResponsiveAppBar tipo={tipo} user={user} setUser={setUser} />
                            <Grid container>
                                {
                                    apelaciones.length > 0
                                        ? apelaciones.map((ap) =>
                                            <Grid sx={{ p:2 }} xs={12} md={6} lg={4} item key={ap.id}>
                                                <OutlinedCard setRevision={setRevision} apelacion={ap} />
                                            </Grid>
                                        )
                                        :<Typography variant="subtitle1" sx={{ margin: '0 auto' }}> No hay apelaciones por el momento. </Typography>
                                }
                            </Grid>
                            {
                                infraccion === null
                                    ? <div></div>
                                    : <div> <Revision apelacion={apelacion} infraccion={infraccion} aprobar={aprobarApelacion} rechazar={rechazarApelacion} /> </div>
                            }
                        </Stack>
                    </>
            }
        </div>
    );
}

export default Juez