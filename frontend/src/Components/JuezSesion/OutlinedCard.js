import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import utility from '../../Utility/utility';

export default function OutlinedCard({ apelacion, setRevision }) {
  return (
    <Box>
      <Card variant="outlined" >
        <React.Fragment>
          <CardContent>
            <Typography variant="h5" component="div">
              Infracción ID-{apelacion.id_infraccion}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              apelada el {utility.invertirString(apelacion.fecha_apelacion.substring(0, 10))}
            </Typography>
          </CardContent>
          <CardActions sx={{ justifyContent: "flex-end" }}>
            <Button
              variant='outlined'
              sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}
              onClick={() => setRevision(apelacion)}
            >
              Revisar
            </Button>
          </CardActions>
        </React.Fragment>
      </Card>
    </Box>
  );
}