import * as React from 'react'

import { Box, Button, TextField, Stack, Typography, Grid } from "@mui/material"

import utility from "../../Utility/utility"

const Revision = ({ apelacion, infraccion, aprobar, rechazar }) => {
    const [observacionInput, setObservacionInput] = React.useState('')
    const [multaInput, setMultaInput] = React.useState('')

    const handleObservacionInput = (event) => setObservacionInput(event.target.value)
    const handleMultaInput = (event) => setMultaInput(event.target.value)

    const handleAprobar = () => {
        aprobar(observacionInput, multaInput)
        setObservacionInput('')
        setMultaInput('')
    }

    const handleRechazar = () => {
        rechazar(observacionInput)
        setObservacionInput('')
        setMultaInput('')
    }

    return (
        <Grid container justifyContent={"space-evenly"}>
            <Grid sx={{ p: 4 }} item xs={12} md={6}>
                <Typography variant="h5" sx={{ fontFamily: 'monospace', fontWeight: 700, mb:4 }}> Revisión Infracción ID-{infraccion.id} </Typography>
                <Stack>
                    <TextField
                        value={observacionInput}
                        onChange={handleObservacionInput}
                        id="apelacion-observacion"
                        sx={{ mb: 2 }}
                        label="Observación"
                        type="text"
                        multiline
                        required
                        placeholder="..."
                        variant="outlined"
                    />
                    <TextField
                        value={multaInput}
                        onChange={handleMultaInput}
                        id="infraccion-monto"
                        sx={{ mb: 2 }}
                        label="Nuevo Multa"
                        placeholder={infraccion.monto}
                        variant="outlined"
                        type="number"
                    />
                    <Box display={"flex"} justifyContent={"space-between"}>
                        <Button onClick={handleAprobar} color="success" variant="contained"> Aprobar </Button>
                        <Button onClick={handleRechazar} color="error" variant="contained"> Rechazar </Button>
                    </Box>
                </Stack>
            </Grid>
            <Grid sx={{ p: 4 }} item xs={12} md={6}>
                <Stack>
                    <Typography variant="h5" sx={{ fontFamily: 'monospace', fontWeight: 700, mb: 2 }}> Detalles Infracción ID-{infraccion.id} </Typography>
                    <Typography> RUT: {infraccion.rut_infractor} </Typography>
                    <Typography> Tipo: {infraccion.tipo} </Typography>
                    <Typography> Multa: {infraccion.monto} (UTM) </Typography>
                    <Typography sx={{ mb: 2 }}> Vencimiento: {utility.invertirString(apelacion.fecha_apelacion.substring(0, 10))} </Typography>
                    <Typography variant="h6" sx={{ fontFamily: 'monospace', fontWeight: 700 }}> Detalle Apelación </Typography>
                    <Typography> {apelacion.detalle} </Typography>
                </Stack>
            </Grid>
        </Grid>
    )
}

export default Revision