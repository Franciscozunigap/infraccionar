import * as React from 'react'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CssBaseline from '@mui/material/CssBaseline'
import TextField from '@mui/material/TextField'
import Box from '@mui/material/Box'
import LocalPoliceIcon from '@mui/icons-material/LocalPoliceOutlined'
import AttachMoneyIcon from '@mui/icons-material/AttachMoneyOutlined';
import GavelIcon from '@mui/icons-material/GavelOutlined';
import Container from '@mui/material/Container'
import { createTheme, ThemeProvider } from '@mui/material/styles'

import { Link } from 'react-router-dom'

import usuarioService from '../Services/Usuarios'

// TODO remove, this demo shouldn't need to reset the theme.

const defaultTheme = createTheme();

const Login = ({ setUser, tipo }) => {
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  const handleSubmit = async (event) => {
    event.preventDefault();

    const loggedUser = await usuarioService.login(tipo, email, password)
    setPassword('')
    setEmail('')
    setUser(loggedUser)
    window.localStorage.setItem('loggedUser', JSON.stringify(loggedUser))
  };

  let icon;

  switch (tipo) {
    case 'inspector': {
      icon = <LocalPoliceIcon />
      break
    }
    case 'cajero': {
      icon = <AttachMoneyIcon />
      break
    }
    case 'juez': {
      icon = <GavelIcon />
      break
    }
    default: {
      break
    }
  }

  return (
    <ThemeProvider theme={defaultTheme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'green' }}>
            {icon}
          </Avatar>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              type='email'
              fullWidth
              id="email"
              label="E-mail"
              autoComplete="email"
              autoFocus
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              label="Contraseña"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2, fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem', fontSize: 16 }}
            >
              Entrar
            </Button>
            <Link to='/'> <Button fullWidth sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem', fontSize: 16 }}> Volver </Button> </Link>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

export default Login;