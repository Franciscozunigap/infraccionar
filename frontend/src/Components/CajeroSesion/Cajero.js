import React, { useEffect, useState, useCallback } from 'react'

import { Navigate } from 'react-router-dom'

import Login from '../Login';
import ResponsiveAppBar from '../ResponsiveAppBar'

import infraccionService from '../../Services/Infracciones'
import apelacionService from '../../Services/Apelaciones';
import utility from '../../Utility/utility'

import { Stack, TableContainer, Paper, Table, TableBody, TableHead, TableRow, TableCell, Button } from '@mui/material'
import { tableCellClasses } from '@mui/material/TableCell'
import { styled } from '@mui/material/styles'

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.info.dark,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const Cajero = () => {
    const [user, setUser] = useState(null)
    const [infracciones, setInfracciones] = useState([])
    const tipo = 'cajero'

    const getAllInfracciones = useCallback(async () => {
        if (user) {
            const allInfracciones = await infraccionService.getCaja()
            const allInfracciones2  = allInfracciones === null ? [] : allInfracciones.reverse()
            setInfracciones(allInfracciones2)
        }
    }, [user])

    useEffect(() => {
        const loggedUserJSON = window.localStorage.getItem('loggedUser')
        if (loggedUserJSON) {
            const user = JSON.parse(loggedUserJSON)
            setUser(user)
        }
    }, [setUser])

    useEffect(() => {
        if (user) {
            getAllInfracciones()
        }
    }, [user, getAllInfracciones])

    if (user) {
        if (user.tipo !== tipo) {
            return <Navigate to='/' />
        }
    }

    const updateEstadoPagado = async (idInfraccion) => {
        await infraccionService.updateEstado(idInfraccion, 'pagado')
        await apelacionService.setRevisada(idInfraccion)
        setInfracciones(infracciones.filter(infraccion => infraccion.id !== idInfraccion))
    }

    return (
        <div className="App">
            {
                user === null
                    ?
                    <Login tipo={tipo} setUser={setUser} />
                    :
                    <>
                        <Stack
                            direction="column"
                            justifyContent="flex-start"
                            alignItems="center"
                            spacing={4}
                            margin={'0 auto'}
                        >
                            <ResponsiveAppBar tipo={tipo} user={user} setUser={setUser} />
                            <Stack>
                                <TableContainer component={Paper}>
                                    <Table sx={{ minWidth: 650 }} aria-label="customized table">
                                        <TableHead>
                                            <TableRow>
                                                <StyledTableCell> ID </StyledTableCell>
                                                <StyledTableCell align="center"> Patente&nbsp; </StyledTableCell>
                                                <StyledTableCell align="center"> Tipo&nbsp; </StyledTableCell>
                                                <StyledTableCell align="center"> Monto (UTM)&nbsp; </StyledTableCell>
                                                <StyledTableCell align="center"> Vence&nbsp; </StyledTableCell>
                                                <StyledTableCell align="center"> Estado&nbsp; </StyledTableCell>
                                                <StyledTableCell align='center'> </StyledTableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {infracciones.map((infraccion) => {
                                                return (
                                                    <StyledTableRow
                                                        key={infraccion.id}
                                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                                    >
                                                        <StyledTableCell component="th" scope="row"> {infraccion.id} </StyledTableCell>
                                                        <StyledTableCell align='center'> {infraccion.patente} </StyledTableCell>
                                                        <StyledTableCell align="center"> {infraccion.tipo} </StyledTableCell>
                                                        <StyledTableCell align="center"> {infraccion.monto} </StyledTableCell>
                                                        <StyledTableCell align="center"> {utility.invertirString(infraccion.fecha_vencimiento.substring(0, 10))} </StyledTableCell>
                                                        <StyledTableCell align="center"> {infraccion.estado} </StyledTableCell>
                                                        <StyledTableCell align='center'>
                                                            <Button
                                                                variant='outlined'
                                                                onClick={() => updateEstadoPagado(infraccion.id)}
                                                                sx={{ fontFamily: 'monospace', fontWeight: 700, letterSpacing: '.1rem' }}
                                                            >
                                                                Pagar
                                                            </Button>
                                                        </StyledTableCell>
                                                    </StyledTableRow>
                                                )
                                            }
                                            )}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Stack>
                        </Stack>
                    </>
            }
        </div>
    );
}

export default Cajero
