import axios from 'axios';

const baseUrl = 'http://localhost:80/apelacion'

const getJuzgado = async () => {
	try {
		const response = await axios.get(`${baseUrl}/juzgado`)
		return response.data

	} catch (error) {
		console.log('Error en la solicitud:', error)
		return null
	}
}

const create = async (newObject) => {
	try {
		const config = {
			headers: { 'apelacion': JSON.stringify(newObject), },
		}
		const response = await axios.post(`${baseUrl}/generator`, null, config)
		return response.data

	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const setRevisada = async (id) => {
	try {
		const response = await axios.put(`${baseUrl}/revisada/${id}`)
		return response.data
	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const aprobarApelacion = async (newObject) => {
	try {
		const config = {
			headers: { 'revision': JSON.stringify(newObject) }
		}
		const response = await axios.put(`${baseUrl}/aprobada`, null, config)
		return response.data

	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const rechazarApelacion = async (newObject) => {
	try {
		const config = {
			headers: { 'revision': JSON.stringify(newObject) }
		}
		const response = await axios.put(`${baseUrl}/rechazada`, null, config)
		return response.data

	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const apelacionService = { create, getJuzgado, setRevisada, aprobarApelacion, rechazarApelacion }

export default apelacionService;