import axios from 'axios';

const baseUrl = 'http://localhost:80/data'

const getCaja = async () => {
	try {
		const response = await axios.get(`${baseUrl}/caja`)
		return response.data

	} catch (error) {
		console.log('Error en la solicitud:', error)
		return null
	}
}

const getSingle = async (id) => {
	try {
		const response = await axios.get(`${baseUrl}/singledata/${id}`)
		return response.data

	} catch (error) {
		console.log('Error en la solicitud:', error)
		return null
	}
}

const getByUserID = async (id) => {
	try {
		const response = await axios.get(`${baseUrl}/userdata/${id}`)
		return response.data

	} catch (error) {
		console.log('Error en la solicitud:', error)
		return null
	}
}

const getByRut = async (rut) => {
	try {
		const response = await axios.get(`${baseUrl}/mydata/${rut}`)
		return response.data

	} catch (error) {
		console.log('Rut no disponible en la base de datos.', error)
		return null
	}
}

const create = async (newObject) => {
	try {
		const config = {
			headers: { 'infraccion': JSON.stringify(newObject), },
		}
		const response = await axios.post(`${baseUrl}/generator`, null, config)
		return response.data

	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const updateEstado = async (id, state) => {
	try {
		switch (state) {
			case 'apelado': {
				const response = await axios.put(`${baseUrl}/mydata/${state}/${id}`)
				return response.data
			}
			case 'pagado': {
				const response = await axios.put(`${baseUrl}/mydata/${state}/${id}`)
				return response.data
			}
			default: {
				break
			}
		}
	} catch (error) {
		console.error('Error en la solicitud:', error)
		throw error
	}
}

const infraccionService = { getCaja, getSingle, getByUserID, getByRut, create, updateEstado }

export default infraccionService;