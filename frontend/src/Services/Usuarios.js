import axios from 'axios';

const baseUrl = 'http://localhost:80/user'

const login = async (tipo, correo, contraseña) => {
	try {
		const response = await axios.get(`${baseUrl}/${tipo}/${correo}/${contraseña}`)
		return response.data

	} catch (error) {
		console.log('Usuario o contraseña incorrectos.', error)
		return null
	}
}

const usuarioService = { login }

export default usuarioService;