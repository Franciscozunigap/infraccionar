const invertirString = (str) => {
    const subString = str.split('-')
    return subString[2] + '-' + subString[1] + '-' + subString[0]
}

const utility = { invertirString }

export default utility