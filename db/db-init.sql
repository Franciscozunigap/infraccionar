CREATE TABLE IF NOT EXISTS infracciones (
    id serial PRIMARY KEY,
    rut_infractor VARCHAR(20),
    email_infractor VARCHAR(100),
    motivo VARCHAR(100),
    ubicacion VARCHAR(100),
    monto DECIMAL(10,2),
    estado VARCHAR(20),
    id_trabajador INT,
    patente VARCHAR(10),
    fecha_infraccion DATE,
    fecha_vencimiento DATE,
    fecha_pago DATE,
    tipo VARCHAR(20)
);

CREATE TABLE IF NOT EXISTS trabajadores (
    id SERIAL PRIMARY KEY,
    nombre VARCHAR(50),
    correo VARCHAR(50),
    tipo VARCHAR(20),
    contrasena VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS apelaciones (
    id SERIAL PRIMARY KEY,
    fecha_apelacion DATE,
    id_infraccion INT,
    detalle VARCHAR(100),
    observacion VARCHAR(100),
    revisada BOOLEAN DEFAULT FALSE
);

INSERT INTO trabajadores (nombre, correo, tipo, contrasena)
VALUES ('Francisco Zuñiga', 'francisco.zunigap@usm.cl', 'inspector', 'inspector');

INSERT INTO trabajadores (nombre, correo, tipo, contrasena)
VALUES ('Francisco Castillo', 'francisco.castilloh@usm.cl', 'cajero', 'cajero');

INSERT INTO trabajadores (nombre, correo, tipo, contrasena)
VALUES ('Agustin Lopez', 'agustin.lopez@usm.cl', 'juez', 'juez');

INSERT INTO infracciones (rut_infractor, email_infractor, motivo, ubicacion, monto, estado, id_trabajador, patente, fecha_infraccion, tipo, fecha_vencimiento)
VALUES ('21040943-2', 'francisco.castilloh@usm.cl', 'mal estacionado', 'calle San Juan', 10, 'emitido', 1, 'JK J6 11', CURRENT_DATE, 'leve', CURRENT_DATE + INTERVAL '60 days');

INSERT INTO infracciones (rut_infractor, email_infractor, motivo, ubicacion, monto, estado, id_trabajador, patente, fecha_infraccion, tipo, fecha_vencimiento)
VALUES ('21040943-2', 'francisco.castilloh@usm.cl', 'cruza con luz roja', 'calle Las cebras', 7, 'emitido', 1, 'JK J6 11', CURRENT_DATE, 'moderada', CURRENT_DATE + INTERVAL '60 days');