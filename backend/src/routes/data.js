var express = require('express');
var router = express.Router();
const pool = require('../db')
const nodemailer = require('../nodemailer')
const { email } = require('../config')

router.get('/caja', (req, res, next) => {
	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('SELECT * FROM infracciones WHERE estado <> $1', ['pagado'])
				client.release()
				if (data && data.rows.length > 0) {
					console.log(data.rows)
					res.status(200).send(data.rows)
				} else {
					res.status(200).send([])
				}
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor')
			}
		})
		.catch((error) => {
			console.error('Error:', error)
			res.status(500).send('Error interno del servidor')
		})
})

router.get('/singledata/:id_infraccion', (req, res, next) => { // Leer infracciones por ID de trabajador
	const id_infraccion = parseInt(req.params.id_infraccion); // Obtener el ID del trabajador de los parámetros de la URL

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('SELECT * FROM infracciones WHERE id = $1', [id_infraccion]);
				client.release();
				if (data && data.rows.length > 0) {
					console.log(data.rows);
					res.status(200).send(data.rows)
				} else {
					res.status(304).send(null)
				}
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
});

router.get('/userdata/:id_trabajador', (req, res, next) => { // Leer infracciones por ID de trabajador
	const id_trabajador = parseInt(req.params.id_trabajador); // Obtener el ID del trabajador de los parámetros de la URL

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('SELECT * FROM infracciones WHERE id_trabajador = $1', [id_trabajador]);
				client.release();
				if (data && data.rows.length > 0) {
					console.log(data.rows);
					res.status(200).send(data.rows)
				} else {
					res.status(304).send([])
				}
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
});

router.get('/mydata/:rutinfractor', (req, res, next) => { // Leer infracciones por ID de trabajador
	const rut_infractor = req.params.rutinfractor; // Obtener el ID del trabajador de los parámetros de la URL

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('SELECT * FROM infracciones WHERE rut_infractor = $1', [rut_infractor]);
				client.release();
				if (data && data.rows.length > 0) {
					// Se encontraron registros para el infractor especificado
					console.log(data.rows);
					res.status(200).send(data.rows);
				} else {
					// No se encontraron registros para el infractor especificado
					res.status(404).send('No se encontraron infracciones para el rut especificado.');
				}
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
});

router.put('/mydata/apelado/:idinfraccion', (req, res, next) => {
	const id_infraccion = req.params.idinfraccion;

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('UPDATE infracciones SET estado = $1 WHERE id = $2', ['apelado', id_infraccion]);
				client.release();
				console.log('Registro actualizado correctamente:', data.rows);
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
})

router.put('/mydata/pagado/:idinfraccion', (req, res, next) => {
	const id_infraccion = req.params.idinfraccion
	const fechaCreacion = new Date()

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('UPDATE infracciones SET estado = $1 WHERE id = $2', ['pagado', id_infraccion])
				console.log('Registro actualizado correctamente:', data.rows);
				const data2 = await client.query('UPDATE infracciones SET fecha_pago = $1 WHERE id = $2', [fechaCreacion,id_infraccion])
				console.log('Registro actualizado correctamente:', data2.rows)
				client.release()
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
})

router.post('/generator', (req, res) => { // Registrar infracción
	const nuevoRegistro = JSON.parse(req.headers.infraccion);

	// Calcular la fecha de vencimiento
	const fechaCreacion = new Date();
	const fechaVencimiento = new Date(fechaCreacion);
	fechaVencimiento.setDate(fechaCreacion.getDate() + 60);

	console.log(nuevoRegistro)

	// Consulta SQL para insertar el nuevo registro en la tabla 'infracciones' con la fecha de vencimiento calculada
	const query = `
		INSERT INTO infracciones (rut_infractor, email_infractor, motivo, ubicacion, monto, estado, id_trabajador, patente, fecha_infraccion, tipo, fecha_vencimiento)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)
		RETURNING *;`

	const array = [
		nuevoRegistro.rut_infractor,
		nuevoRegistro.email_infractor,
		nuevoRegistro.motivo,
		nuevoRegistro.ubicacion,
		nuevoRegistro.monto,
		nuevoRegistro.estado,
		nuevoRegistro.id_trabajador,
		nuevoRegistro.patente,
		fechaCreacion,
		nuevoRegistro.tipo,
		fechaVencimiento // Agregamos la fecha de vencimiento calculada
	]

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query(query, array);
				const info = await nodemailer.sendMail({
					from: `"Municipalidad San Benito" <${email.user}>`,
					to: nuevoRegistro.email_infractor,
					subject: 'Infracción cometida',
					html: `<div>
						<p>Has cometido una <strong>infracción vehicular</strong>!</p>
						<p> patente: ${nuevoRegistro.patente} </p>
						<p> multa: ${nuevoRegistro.monto} UTM, vence el ${fechaVencimiento} </p>
						<p> motivo: ${nuevoRegistro.motivo} </p>
						<p> ubicación: ${nuevoRegistro.ubicacion} </p>
					</div>`
				})
				console.log("Message sent: %s", info.messageId);
				client.release();
				console.log('Registro insertado correctamente:', data.rows);
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error al insertar registro:', error);
				res.status(500).send('Error al insertar registro');
			}
		}
		)
		.catch(error => {
			console.error('Error al insertar registro:', error);
			res.status(500).send('Error al insertar registro');
		})
});

module.exports = router;





