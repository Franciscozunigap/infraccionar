var express = require('express');
var router = express.Router();
const pool = require('../db')

router.post('/generator', (req, res, next) => {
	const nuevaApelacion = JSON.parse(req.headers.apelacion);

	const fecha_apelacion = new Date()

	console.log(nuevaApelacion)

	const query = `
		INSERT INTO apelaciones (fecha_apelacion, id_infraccion, detalle, revisada)
		VALUES ($1, $2, $3, FALSE)
		RETURNING *;
		`

	const array = [
		fecha_apelacion,
		nuevaApelacion.id_infraccion,
		nuevaApelacion.detalle
	]

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query(query, array);
				client.release();
				console.log('Apelacion insertada correctamente:', data.rows);
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error al insertar Apelacion:', error);
				res.status(500).send('Error al insertar Apelacion');
			}
		}
		)
		.catch(error => {
			console.error('Error al insertar Apelacion:', error);
			res.status(500).send('Error al insertar Apelacion');
		})
})

router.put('/revisada/:idinfraccion', (req, res, next) => {
	const id_infraccion = req.params.idinfraccion;

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('UPDATE apelaciones SET revisada = true WHERE id_infraccion = $1', [id_infraccion]);
				client.release();
				console.log('Registro actualizado correctamente:', data.rows);
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
})

router.put('/rechazada', (req, res, next) => {
	const { id, id_infraccion, observacion } = JSON.parse(req.headers.revision)

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('UPDATE apelaciones SET observacion = $1, revisada = true WHERE id = $2', [observacion, id]);
				const data2 = await client.query('UPDATE infracciones SET estado = $1 WHERE id = $2;', ['por pagar', id_infraccion])
				client.release();
				console.log('Registro actualizado correctamente:', data.rows, data2.rows);
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
})

router.put('/aprobada', (req, res, next) => {
	const { id, id_infraccion, observacion, monto } = JSON.parse(req.headers.revision);

	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('UPDATE apelaciones SET observacion = $1, revisada = true WHERE id = $2;', [observacion, id]);
				const data2 = await client.query('UPDATE infracciones SET monto = $1, estado = $2 WHERE id = $3;', [monto, 'por pagar', id_infraccion])
				client.release();
				console.log('Registro actualizado correctamente:', data.rows, data2.rows)
				res.status(200).send('OK');
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor');
			}
		}
		)
		.catch((error) => {
			console.error('Error:', error);
			res.status(500).send('Error interno del servidor');
		})
})

router.get('/juzgado', (req, res, next) => {
	pool.connect()
		.then(async (client) => {
			try {
				const data = await client.query('SELECT * FROM apelaciones WHERE revisada = false')
				client.release()
				if (data && data.rows.length > 0) {
					console.log(data.rows)
					res.status(200).send(data.rows)
				} else {
					res.status(200).send([])
				}
			} catch (error) {
				client.release();
				console.error('Error:', error);
				res.status(500).send('Error interno del servidor')
			}
		})
		.catch((error) => {
			console.error('Error:', error)
			res.status(500).send('Error interno del servidor')
		})
})

module.exports = router;