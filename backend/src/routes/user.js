var express = require('express');
var router = express.Router();
const pool = require('../db')

router.get('/:tipo/:correo/:contrasena', (req, res, next) => {
    const correo = req.params.correo;
    const contraseña = req.params.contrasena;
    const tipo = req.params.tipo

    pool.connect()
        .then(async (client) => {
            try {
                const data = await client.query('SELECT * FROM trabajadores WHERE correo = $1 AND contrasena = $2 AND tipo = $3 LIMIT 1;', [correo, contraseña, tipo]);
                client.release();
                if (data && data.rows.length > 0) {
                    console.log(data.rows[0]);
                    res.status(200).send(data.rows[0]);
                } else {
                    res.status(404).send('No se encontró ningún trabajador con el correo y contraseña especificados.');
                }
            } catch (error) {
                client.release();
                console.error('Error:', error);
                res.status(500).send('Error interno del servidor');
            }
        })
        .catch((error) => {
            console.error('Error:', error);
            res.status(500).send('Error interno del servidor');
        });
});

module.exports = router;