const nodemailer = require('nodemailer')
const { email } = require('./config')

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        // Variables a cambiar
        user: email.user,
        pass: email.pass,
    },
})

transporter
    .verify()
    .then(() => console.log('Gmail guardado con éxito'))
    .catch(error => console.log(error))

module.exports = transporter