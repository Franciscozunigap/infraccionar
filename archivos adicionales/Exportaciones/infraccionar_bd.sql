PGDMP     ,                	    {           infraccionar    10.23    10.23     �
           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �
           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �
           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �
           1262    16393    infraccionar    DATABASE     �   CREATE DATABASE infraccionar WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Spain.1252' LC_CTYPE = 'Spanish_Spain.1252';
    DROP DATABASE infraccionar;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �
           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �
           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    16452    infracciones    TABLE     �  CREATE TABLE public.infracciones (
    id integer NOT NULL,
    rut_infractor character varying(20),
    motivo character varying(100),
    ubicacion character varying(100),
    monto numeric(10,2),
    estado character varying(20),
    id_trabajador numeric(10,2),
    patente character varying(10),
    fecha_infraccion character varying(20),
    tipo character varying(20),
    fecha_vencimiento date
);
     DROP TABLE public.infracciones;
       public         postgres    false    3            �            1259    16450    infracciones_id_seq    SEQUENCE     �   CREATE SEQUENCE public.infracciones_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.infracciones_id_seq;
       public       postgres    false    3    197                        0    0    infracciones_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.infracciones_id_seq OWNED BY public.infracciones.id;
            public       postgres    false    196            �            1259    16482    inspectores    TABLE     �   CREATE TABLE public.inspectores (
    id integer NOT NULL,
    nombre character varying(255),
    correo character varying(255),
    rut character varying(12),
    id_trabajador integer,
    "contraseña" integer
);
    DROP TABLE public.inspectores;
       public         postgres    false    3            �            1259    16480    inspectores_id_seq    SEQUENCE     �   CREATE SEQUENCE public.inspectores_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.inspectores_id_seq;
       public       postgres    false    199    3                       0    0    inspectores_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.inspectores_id_seq OWNED BY public.inspectores.id;
            public       postgres    false    198            u
           2604    16455    infracciones id    DEFAULT     r   ALTER TABLE ONLY public.infracciones ALTER COLUMN id SET DEFAULT nextval('public.infracciones_id_seq'::regclass);
 >   ALTER TABLE public.infracciones ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196    197            v
           2604    16485    inspectores id    DEFAULT     p   ALTER TABLE ONLY public.inspectores ALTER COLUMN id SET DEFAULT nextval('public.inspectores_id_seq'::regclass);
 =   ALTER TABLE public.inspectores ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    199    199            �
          0    16452    infracciones 
   TABLE DATA               �   COPY public.infracciones (id, rut_infractor, motivo, ubicacion, monto, estado, id_trabajador, patente, fecha_infraccion, tipo, fecha_vencimiento) FROM stdin;
    public       postgres    false    197   �       �
          0    16482    inspectores 
   TABLE DATA               \   COPY public.inspectores (id, nombre, correo, rut, id_trabajador, "contraseña") FROM stdin;
    public       postgres    false    199   �                  0    0    infracciones_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.infracciones_id_seq', 3, true);
            public       postgres    false    196                       0    0    inspectores_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.inspectores_id_seq', 10, true);
            public       postgres    false    198            x
           2606    16457    infracciones infracciones_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.infracciones
    ADD CONSTRAINT infracciones_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.infracciones DROP CONSTRAINT infracciones_pkey;
       public         postgres    false    197            z
           2606    16490    inspectores inspectores_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.inspectores
    ADD CONSTRAINT inspectores_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.inspectores DROP CONSTRAINT inspectores_pkey;
       public         postgres    false    199            �
   �   x�}��n1E��W��1�j��@��xF+^�v|}vI�(��iι�ZS�U�	���>i}��|`b��h�[�(2ip3c@�0N`�g��-��y=G����S̬�L�l�p��*�����Љ�!�(�"�,��y;�����3uU�8.�8<�O��c�w�uꂜ���4w7z�7�M<.e�Q�ZgJ�/�O�      �
   �   x�e�;NAE��yl��e�2V@� #�Qg�u��*�J�YOW�z����$����}��?��~�x�xX/Q��:�.@��Vpn��&�l�0�@0['���8L��lIm�6@[@Cek��� ��9�*L9A�Hd�4��j��PH�~}h���}���S��k:	��㙎�vK+����獈0j�     