# Escuadrón Tortilla

Este es el repositorio del *Grupo Escuadrón Tortilla*, cuyos integrantes son:

* Francisco Castillo - 202130514-9
* Agustín López - 202130549-1
* Francisco Zúñiga - 202130509-2
* **Tutor**: Tabata Ahumada

## Wiki

Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/Franciscozunigap/infraccionar/-/wikis/home)

## Videos

* [Video presentación cliente](https://youtu.be/k4P0wrC-Qrw)
* [Video presentación software](https://youtu.be/rRmCE2Wy5Qk)

## Aspectos técnicos relevantes

Para utilizar el software, se debe tener instalado el programa [Docker Desktop](https://www.docker.com/products/docker-desktop/) y la versión 18.17 de [Node.js](https://nodejs.org/en), luego seguir los pasos a continuación:


Dirigirse al directorio **./backend** y crear un archivo `.env` en el cúal deben ir las variables de entorno escritas en el archivo `.env.example`.

Dirigirse al directorio **./** del proyecto.

Para crear la imagen del contenedor, ejecutar el comando

```text
docker-compose build
```

Para levantar el contenedor, ejecutar el comando

```text
docker-compose up
```

Para detener el contenedor, ejecutar el comando

```text
docker-compose down
```

## Modo de uso

Ir a [http://localhost:3000/](https://localhost) para ingresar al software.

Para Inspector, inciar sesión con

```text
francisco.zunigap@usm.cl
```
```text
inspector
```

Para Cajero, inciar sesión con

```text
francisco.castilloh@usm.cl
```
```text
cajero
```

Para Juez, iniciar sesión con

```text
agustin.lopez@usm.cl
```
```text
juez
```

**Importante:** Las tablas de la base de datos inicialmente están vacías, por lo que se requiere ingresar las primeras infracciones. 
